# Bizfly 2FA Demo

## Requirement
- npm version 6.4.1+
- nodejs version 10.15.3+
- ubuntu 16.04+

## Setup
```
git clone https://gitlab.com/tringuyen5835/bizfly_2fa_demo_frontend.git
```

```
cd bizfly_2fa_demo_frontend
```

```
npm install
```

## Configuration
Create .env file
```.env
VUE_APP_VUEX_KEY=bizfly_2fa_demo
VUE_APP_API_URL=http://localhost:5000
VUE_APP_WEB_URL=http://localhost:8080
VUE_APP_POPUP_SRC=https://otp.ss-hn-1.vccloud.vn/v1/dev/popup.js
```

## Run / Build
- Run dev
```
npm run serve
```

- Build
```
npm run build
```
