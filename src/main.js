import Vue from 'vue'
import {mapGetters, mapState} from 'vuex'

import App from './app'
import router from './router'
import store from './store'

// import handle_service_worker from './service-worker-handlers'

import Notifications from 'vue-notification'

import services from './services'

import vuetify from './plugins/vuetify'

// handle_service_worker(store)

Vue.config.productionTip = false

Vue.use(services)
Vue.use(Notifications)

router.beforeEach((to, _from, next) => {
  const access_token = store.state.User.access_token
  const is_required_auth = to.matched.some(route => route.meta.auth)
  if (is_required_auth && !access_token) {
    next({
      path: '/auth',
      query: {
        callback: to.fullPath
      }
    })
    return
  }
  next()
})

Vue.mixin({
  computed: {
    ...mapGetters('Common', ['common_data']),
    ...mapGetters('User', ['auth_user', 'access_token']),
    ...mapState('Device', {
      device_screen_size: 'screen_size',
      locale: 'locale'
    }),
  }
})

new Vue({
  vuetify,
  router,
  store,
  created () {
    this.$services.context = this
  },
  render: h => h(App)
}).$mount('#app')


