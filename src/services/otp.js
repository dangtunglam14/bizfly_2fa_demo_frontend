export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  generate_session (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/otp/generate-session', request)
  }

  verify (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/otp/verify', request)
  }

  send (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/otp/send', request)
  }

  confirm_session (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/otp/confirm-session', request)
  }
}
