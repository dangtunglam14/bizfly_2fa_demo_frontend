export default {
  namespaced: true,
  state: {
    data: {
      max_content_length: 15 * 1024 * 1024,
      date_format: 'DD/MM/YYYY',
      email_regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      digit_regex: /^\d+$/,
    }
  },
  getters: {
    common_data: state => state.data
  }
}
