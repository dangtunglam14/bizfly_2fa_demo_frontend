export default {
  namespaced: true,
  state: {
    screen_size: 'lg',
    width: null,
    height: null,
    current_location: null,
    online: true,
    background: false,
    locale: 'vi'
  },
  mutations: {
    locale (state, value) {
      state.locale = value
    },
    screen_resized (state, {width, height, size}) {
      state.width = width
      state.height = height
      state.screen_size = size
    },
    current_location (state, data) {
      state.current_location = data
    },
    online (state, value) {
      state.online = value
    },
    background (state, value) {
      state.background = value
    }
  },
  actions: {},
  getters: {}
}
