export default {
  convert_string_to_date: (string, format) => {
    if (!string) return null
    return moment(string, format)
  },
  convert_date_format: (string, from_format, to_format) => {
    if (!string) return null
    const datetime = moment(string, from_format)
    return datetime.format(to_format)
  },
  get_current_month_to_now: (format) => {
    if (!format) format = 'YYYY-MM-DD'
    let now = moment()
    let start_of_month = now.clone().startOf('month')
    return [start_of_month.format(format), now.format(format)]
  },
  get_window_size: window_width => {
    if (window_width > 1904) return 'xl'
    if (window_width > 1264) return 'lg'
    if (window_width > 960) return 'md'
    if (window_width > 600) return 'sm'
    return 'xs'
  },
  dive_to_path (path, data) {
    let root = path.split('.')
    const dive = (_data) => {
      let result = _data
      if (root.length > 0) {
        let key = root.splice(0, 1)
        return dive(result[key])
      }
      return result
    }
    return dive(data)
  },
  get_object_translation (locale, object) {
    let trans = object.translations || []
    return trans.find(t => t.locale === locale)
  }
}